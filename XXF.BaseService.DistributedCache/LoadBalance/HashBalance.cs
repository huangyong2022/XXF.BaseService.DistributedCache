﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.DistributedCache.Storage;

namespace XXF.BaseService.DistributedCache.LoadBalance
{
    public class HashBalance : IBalance
    {
        public string ChooseServer(List<string> serviceconfiglist, string key) 
        {
            if (serviceconfiglist == null||serviceconfiglist.Count==0)
                return null;
            return serviceconfiglist[Math.Abs(key.GetHashCode()) % serviceconfiglist.Count];
        }
    }
}
